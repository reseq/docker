FROM debian:jessie-backports AS base

#python
RUN apt-get -y update && apt-get -y install python3

#Bedtools needs python binary, works with python3
RUN ln -s `which python3` /usr/local/bin/python

# Java 8
# need to specify ca-certificates-java to install from backports
RUN apt-get -y update && apt-get -y install --no-install-recommends \
    -t jessie-backports\
    ca-certificates-java\
    openjdk-8-jre-headless\
    perl

FROM base AS builder

# need wget as ADD doesn't support sourceforge redirects
RUN apt-get -y update && apt-get -y install wget\
    unzip\
    bzip2\
    g++\
    gcc\
    libc6-dev\
    make

##Bedtools 2.18.0
FROM builder AS bedtools
#apt-get deps
RUN apt-get -y update && apt-get -y install zlib1g-dev
#Bedtools
RUN wget "https://github.com/arq5x/bedtools2/releases/download/v2.18.0/bedtools-2.18.0.tar.gz" &&\
    tar -xvzf bedtools-2.18.0.tar.gz &&\
    cd bedtools-2.18.0 &&\
    make &&\
    mkdir -p /opt/bedtools-2.18.0/bin &&\
    mv bin/* /opt/bedtools-2.18.0/bin

##Blat
FROM builder AS blat
RUN mkdir -p /opt/blat/bin &&\
    wget -P /opt/blat/bin "http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/blat/blat" &&\
    chmod +x /opt/blat/bin/blat

##Bowtie 2.3.1
FROM builder AS bowtie
#Bowtie
RUN wget "https://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.3.1/bowtie2-2.3.1-linux-x86_64.zip" &&\
    unzip bowtie2-2.3.1-linux-x86_64.zip &&\
    mkdir -p /opt/bowtie2-2.3.1/bin &&\
    cd bowtie2-2.3.1 &&\
    cp bowtie2 bowtie2-align-s bowtie2-align-l bowtie2-build bowtie2-build-s bowtie2-build-l\
        bowtie2-inspect bowtie2-inspect-s bowtie2-inspect-l /opt/bowtie2-2.3.1/bin

##Fastx Toolkit 0.0.13
FROM builder AS fastx
#Fastx
RUN wget "http://hannonlab.cshl.edu/fastx_toolkit/fastx_toolkit_0.0.13_binaries_Linux_2.6_amd64.tar.bz2" &&\
    mkdir -p /opt/fastx_toolkit_0.0.13 &&\
    tar -C /opt/fastx_toolkit_0.0.13 -xvjf fastx_toolkit_0.0.13_binaries_Linux_2.6_amd64.tar.bz2

##HISAT2 2.0.5
FROM builder AS hisat
RUN wget "ftp://ftp.ccb.jhu.edu/pub/infphilo/hisat2/downloads/hisat2-2.0.5-Linux_x86_64.zip" &&\
    unzip hisat2-2.0.5-Linux_x86_64.zip &&\
    mkdir -p /opt/hisat2-2.0.5/bin &&\
    cd hisat2-2.0.5 &&\
    cp hisat2 hisat2-align-s hisat2-align-l hisat2-build hisat2-build-s hisat2-build-l\
        hisat2-inspect hisat2-inspect-s hisat2-inspect-l /opt/hisat2-2.0.5/bin

##Pear 0.9.10
FROM builder AS pear
COPY pear/pear-0.9.10-linux-x86_64.tar.gz .
RUN mkdir -p /opt/pear-0.9.10/bin &&\
    tar -xvzf pear-0.9.10-linux-x86_64.tar.gz &&\
    mv pear-0.9.10-linux-x86_64/bin/pear /opt/pear-0.9.10/bin/

##Samtools 1.2
FROM builder AS samtools
#apt-get deps
RUN apt-get -y update && apt-get -y install zlib1g-dev
#samtools
RUN wget "https://sourceforge.net/projects/samtools/files/samtools/1.2/samtools-1.2.tar.bz2" &&\
    tar -xvjf samtools-1.2.tar.bz2 &&\
    cd samtools-1.2 &&\
    sed -i -- 's/\(-D_CURSES_LIB=\)1/\10/' Makefile &&\
    sed -i -- 's/^\(LIBCURSES=\)/#\1/' Makefile &&\
    make &&\
    make install

##Trimgalore 0.4.4
FROM builder AS trimgalore
#Trimgalore
RUN wget "http://www.bioinformatics.babraham.ac.uk/projects/trim_galore/trim_galore_v0.4.4.zip" &&\
    unzip -d trim_galore_v0.4.4 trim_galore_v0.4.4.zip &&\
    mkdir -p /opt/trim-galore-0.4.4/bin &&\
    cp trim_galore_v0.4.4/trim_galore /opt/trim-galore-0.4.4/bin

##Trinity 2.3.2
FROM builder AS trinity
RUN apt-get -y update && apt-get -y install zlib1g-dev
#Trinity
RUN wget "https://github.com/trinityrnaseq/trinityrnaseq/archive/Trinity-v2.3.2.tar.gz" &&\
    tar -C /opt -xvzf Trinity-v2.3.2.tar.gz &&\
    cd /opt/trinityrnaseq-Trinity-v2.3.2 &&\
    make

##XLWT Python Library
FROM builder AS xlwt
RUN apt-get -y update && apt-get -y install python3-wheel &&\
    wget "https://pypi.python.org/packages/82/a3/33e92fab77d5b12de2d77f2711319b2c2a743901ffa4e80dc987f7de9833/xlwt-1.2.0-py2.py3-none-any.whl#md5=4045ad819f54d808670023023f557791" &&\
    python -m wheel install xlwt-1.2.0-py2.py3-none-any.whl

##Main Image
FROM base

COPY --from=bedtools /opt/bedtools-2.18.0 /opt/bedtools-2.18.0
ENV PATH="/opt/bedtools-2.18.0/bin:${PATH}"

COPY --from=blat /opt/blat /opt/blat
ENV PATH="/opt/blat/bin:${PATH}"

RUN apt-get -y update && apt-get -y install libtbb2
COPY --from=bowtie /opt/bowtie2-2.3.1 /opt/bowtie2-2.3.1
ENV PATH="/opt/bowtie2-2.3.1/bin:${PATH}"

COPY --from=fastx /opt/fastx_toolkit_0.0.13 /opt/fastx_toolkit_0.0.13
ENV PATH="/opt/fastx_toolkit_0.0.13/bin:${PATH}"

COPY --from=hisat /opt/hisat2-2.0.5 /opt/hisat2-2.0.5
ENV PATH="/opt/hisat2-2.0.5/bin:${PATH}"

COPY --from=pear /opt/pear-0.9.10 /opt/pear-0.9.10
ENV PATH="/opt/pear-0.9.10/bin:${PATH}"

COPY --from=samtools /usr/local/bin /usr/local/bin

RUN apt-get -y update && apt-get -y install\
    cutadapt\
    fastqc\
    python3-setuptools
ENV PATH="/opt/trim-galore-0.4.4/bin:${PATH}"
COPY --from=trimgalore /opt/trim-galore-0.4.4 /opt/trim-galore-0.4.4

RUN apt-get -y update && apt-get -y install libgomp1
COPY --from=trinity /opt/trinityrnaseq-Trinity-v2.3.2 /opt/trinityrnaseq-Trinity-v2.3.2
ENV PATH="/opt/trinityrnaseq-Trinity-v2.3.2:${PATH}"

COPY --from=xlwt /usr/local/lib/python3.4/dist-packages/xlwt /usr/local/lib/python3.4/dist-packages/xlwt
COPY --from=xlwt /usr/local/lib/python3.4/dist-packages/xlwt-1.2.0.dist-info /usr/local/lib/python3.4/dist-packages/xlwt-1.2.0.dist-info

##Extra tools for scripts
RUN apt-get -y update && apt-get -y install\
    bc\
    gawk

##ReSeq Control Scripts
ADD control-scripts/* /opt/reseq/bin/
RUN chmod +x /opt/reseq/bin/*
ENV PATH="/opt/reseq/bin:${PATH}"

WORKDIR /data/input

ENTRYPOINT ["spliceprogram.sh"]
CMD ["-h"]
